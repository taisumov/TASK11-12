/* Задания на урок:

1) Удалить все рекламные блоки со страницы (правая часть сайта)

2) Изменить жанр фильма, поменять "комедия" на "драма"

3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
Реализовать только при помощи JS

4) Список фильмов на странице сформировать на основании данных из этого JS файла.
Отсортировать их по алфавиту 

5) Добавить нумерацию выведенных фильмов */

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};


const roundText = (str, num) => {
    let sliced = str.slice(0, num);
    if (sliced.length < str.length) {
        sliced += '...';
    }
    return sliced
}


let ads = document.querySelector('.promo__adv')
ads.parentNode.removeChild(ads)

let comedy = document.querySelector('.promo__genre')
comedy.innerHTML = 'Драма'

let promo = document.querySelector('.promo__bg')
promo.style.background = 'url("./img/bg.jpg")'

let listFilms = document.querySelector('.promo__interactive-list')

const renderFilms = () => {
    while (listFilms.firstChild) {
        listFilms.firstChild.remove()
    }
    movieDB.movies.sort().forEach((el, index) => {
        let x = document.createElement("li")
        x.className = 'promo__interactive-item'
        x.innerHTML = `${index+1} ${roundText(el, 21)} <div class="delete"></div>`
        listFilms.appendChild(x)
    })
    refreshDelete()
}

let refreshDelete = () => {
    let deleted = document.querySelectorAll('.delete')

    deleted.forEach((d, index) => d.addEventListener('click', (e) => {
        e.target.parentNode.parentNode.removeChild(e.target.parentNode)
        movieDB.movies = movieDB.movies.sort().filter((el, ind) => ind != index)
        renderFilms()
    }))
}

renderFilms()
refreshDelete()

let button = document.querySelector('button')
let input = document.querySelector('.adding__input')

button.addEventListener('click', (e) => {
    e.preventDefault()
    if(input.value){
        movieDB.movies.push(input.value)
        renderFilms()
        
    }
    input.value = ''
    let isFav = document.querySelector('input[type=checkbox]');
    if(isFav.checked) console.log('Добавлен любимый фильм!')
})

